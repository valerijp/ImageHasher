use std::{fs, path::Path};
use blockhash::{blockhash144, blockhash16, blockhash256, blockhash64};
use clap::{ArgEnum, Parser, PossibleValue};
use crate::DigestSize::{S144, S256, S64};

#[derive(Clone)]
enum DigestSize {
    S16,
    S64,
    S144,
    S256,
}

impl ArgEnum for DigestSize {
    fn value_variants<'a>() -> &'a [Self] {
        &[DigestSize::S16, S64, S144, S256]
    }

    fn from_str(input: &str, _ignore_case: bool) -> Result<Self, String> {
        match input {
            "16" => Ok(Self::S16),
            "64" => Ok(Self::S64),
            "144" => Ok(Self::S144),
            "256" => Ok(Self:: S256),
            _ => Err(String::from("digest size not supported")),
        }
    }

    fn to_possible_value<'a>(&self) -> Option<PossibleValue<'a>> {
        match self {
            DigestSize::S16 => Some(PossibleValue::new("16")),
            S64 => Some(PossibleValue::new("64")),
            S144 => Some(PossibleValue::new("144")),
            S256 => Some(PossibleValue::new("256")),
        }
    }
}


#[derive(Parser)]
struct Cli {
    /// size of blockhash digest to use
    #[clap(short = 's', long, arg_enum, default_value = "64")]
    digest_size: DigestSize,

    /// rename the file to <hash> - <original_name>.<ext> pattern
    #[clap(short, long)]
    r#move: bool,

    /// instead of prepending hash to the filename will only use the hash
    #[clap(long, requires="move")]
    no_preserve_filename: bool,

    /// instead of moving the file will just print the intended actions
    #[clap(long, requires="move")]
    dry_run: bool,

    /// image to process
    source: String,
}

fn main() {
    let cli = Cli::parse();

    let source = Path::new(&cli.source).canonicalize().unwrap().clone();

    if !source.is_file() {
        eprintln!("please pass a webp file to convert");
        return;
    }

    let img = image::open(source.clone()).expect("could not load image");
    let hash = match cli.digest_size {
        DigestSize::S16 => blockhash16(&img).to_string(),
        S64 => blockhash64(&img).to_string(),
        S144 => blockhash144(&img).to_string(),
        S256 => blockhash256(&img).to_string(),
    };


    if cli.r#move {
        let mut target = source.clone();

        let stem = target.file_stem().unwrap().to_str().unwrap();
        let ext = target.extension().unwrap().to_str().unwrap();

        if cli.no_preserve_filename {
            target.set_file_name(format!("{}.{}", hash, ext));
        } else {
            target.set_file_name(format!("{} - {}.{}", hash, stem, ext));
        }
        if cli.dry_run {
            println!("will move {}", source.to_str().unwrap());
            println!("to {}", target.to_str().unwrap());
        } else {
            if target.exists() {
                eprintln!("target already exists, bailing");
                return;
            }
            fs::rename(source, target).expect("could not move file");
        }
    } else {
        println!("{}", hash)
    }
}
